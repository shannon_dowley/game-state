## General Principles
# Internationalisation
- Game terms, events, and exceptions are designed to be parseable by a locale specific resource bundle
- REST endpoints should convert the message into a locale specific phrase before displaying to user
- Event logs that are stored and retrieved should be stored in a parseable format, such as Event name and arguments.
- User provided chat will be stored with the locale of the user
# Commands
- Commands are designed to be public information only
- Only information exposed in Commands.kt are available for use
- Commands should correspond to a unit of work that affects the game state
- The game id is required to identify the correct aggregate in the event store
- 