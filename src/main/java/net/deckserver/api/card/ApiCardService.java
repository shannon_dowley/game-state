package net.deckserver.api.card;

import com.fasterxml.jackson.annotation.JsonSetter;
import org.jetbrains.annotations.NotNull;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.Objects;

@Service
public class ApiCardService implements CardService {

    WebClient webClient = WebClient.builder()
            .baseUrl("https://api.krcg.org")
            .build();

    @NotNull
    @Override
    @Cacheable("cards")
    public CardDetail getCard(String id) throws CardNotFoundException {
        try {
            return Objects.requireNonNull(webClient.get()
                    .uri("/card/{id}", id)
                    .accept(MediaType.APPLICATION_JSON)
                    .retrieve()
                    .bodyToMono(KrcgCard.class)
                    .map(KrcgCard::toCardDetail)
                    .block());
        } catch (Exception e) {
            throw new CardNotFoundException("Card '" + id + "' not found");
        }
    }

}

record KrcgCard(String id, @JsonSetter("printed_name") String name, int group, Integer capacity,
                List<String> disciplines, String banned) {
    public CardDetail toCardDetail() {
        return new CardDetail(id, name, group, disciplines, capacity, banned != null);
    }
}