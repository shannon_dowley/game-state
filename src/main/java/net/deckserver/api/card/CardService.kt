package net.deckserver.api.card

import kotlin.jvm.Throws

data class CardDetail(val id: String, val name: String, val group: Int?, val disciplines: List<String>?, val capacity: Int?, val banned: Boolean)

interface CardService {
    @Throws(CardNotFoundException::class)
    fun getCard(id: String?) : CardDetail
}

class CardNotFoundException(message: String) : Exception(message)