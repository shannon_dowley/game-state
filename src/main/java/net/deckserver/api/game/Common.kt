package net.deckserver.api.game

enum class RegionType {
    IN_PLAY,
    OUT_OF_PLAY,
    ASH_HEAP,
    REMOVED_FROM_GAME,
    CRYPT,
    LIBRARY,
    UNCONTROLLED,
    HAND,
    RESEARCH
}

enum class PhaseType {
    UNLOCK, MASTER, MINION, INFLUENCE, DISCARD;

    fun next(): PhaseType {
        return when (this) {
            UNLOCK -> MASTER
            MASTER -> MINION
            MINION -> INFLUENCE
            INFLUENCE -> DISCARD
            DISCARD -> UNLOCK
        }
    }
}

enum class DisciplineType(val code: String) {
    ABOMBWE("abo"),
    ANIMALISM("ani"),
    AUSPEX("aus"),
    BLOOD_SORCERY("tha"),
    CELERITY("cel"),
    CHIMERSTRY("chi"),
    DAIMOINON("dai"),
    DEMENTATION("dem"),
    DOMINATE("dom"),
    FORTITUDE("for"),
    MELPOMINEE("mel"),
    MYTHERCERIA("myt"),
    NECROMANCY("nec"),
    OBEAH("obe"),
    OBFUSCATE("obf"),
    OBTENEBRATION("obt"),
    POTENCE("pot"),
    PRESENCE("pre"),
    PROTEAN("pro"),
    QUIETUS("qui"),
    SANGUINUS("san"),
    SERPENTIS("ser"),
    SPIRITUS("spi"),
    TEMPORIS("tem"),
    THANATOSIS("tha"),
    VALEREN("val"),
    VICISSITUDE("vic"),
    VISCERATIKA("vis"),

    // Imbued
    VENGEANCE("ven"),
    DEFENSE("def"),
    INNOCENCE("inn"),
    JUSTICE("jus"),
    MARTYRDOM("mar"),
    REDEMPTION("red"),
    VISION("viz"),

    // Other
    FLIGHT("flight"),
    STRIGA("str"),
    MALEFICIA("mal");

    companion object {
        @JvmStatic
        fun byCode(code: String): DisciplineType {
            for (type: DisciplineType in DisciplineType.values()) {
                if (type.code == code.lowercase()) {
                    return type
                }
            }
            throw IllegalArgumentException("Definition for $code not found")
        }
    }
}