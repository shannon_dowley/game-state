package net.deckserver.api.game.controllers;

import net.deckserver.api.game.command.*;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Locale;

@RestController
@RequestMapping("/api")
public class CommandController {
//
//    private final ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
//    private final CommandGateway gateway;
//
//    public CommandController(CommandGateway gateway) {
//        this.gateway = gateway;
//        messageSource.setBasenames("common", "commands", "events");
//    }
//
//    @PostMapping(value = "/createGame", consumes = "application/json")
//    public String createGame(@RequestBody CreateGame command, Locale locale) {
//        gateway.sendAndWait(command);
//        return messageSource.getMessage(command.getClass().getSimpleName(), new Object[]{command.getGameId(),command.getGameName()}, locale);
//    }
//
//    @PostMapping("/addPlayer")
//    public String addPlayer(@RequestBody AddPlayer command, Locale locale) {
//        gateway.sendAndWait(command);
//        return messageSource.getMessage(command.getClass().getSimpleName(), new Object[]{command.getGameId(), command.getPlayerName()}, locale);
//    }
//
//    @PostMapping("/removePlayer")
//    public String removePlayer(@RequestBody RemovePlayer command, Locale locale) {
//        gateway.sendAndWait(command);
//        return messageSource.getMessage(command.getClass().getSimpleName(), new Object[]{command.getGameId(), command.getPlayerName()}, locale);
//    }
//
//    @PostMapping("/registerDeck")
//    public String registerDeck(@RequestBody RegisterDeck command, Locale locale) {
//        gateway.sendAndWait(command);
//        return messageSource.getMessage(command.getClass().getSimpleName(), new Object[]{command.getGameId(), command.getPlayerName()}, locale);
//    }
//
//    @PostMapping("/startGame")
//    public String startGame(@RequestBody StartGame command, Locale locale) {
//        gateway.sendAndWait(command);
//        return messageSource.getMessage(command.getClass().getSimpleName(), new Object[]{command.getGameId()}, locale);
//    }
//
//    @ExceptionHandler({GameException.class})
//    public ResponseEntity<String> getExceptionMessage(GameException exception, Locale locale) {
//        Object[] resolvedArgs = Arrays.stream(exception.getArgs()).map(o -> resolveArgument(o, locale)).toArray();
//        String message = messageSource.getMessage(exception.getClass().getSimpleName(), resolvedArgs, locale);
//        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
//    }
//
//    private Object resolveArgument(Object arg, Locale locale) {
//        try {
//            return messageSource.getMessage(new DefaultMessageSourceResolvable(new String[]{arg.getClass().getSimpleName(), arg.toString()}), locale);
//        } catch (NoSuchMessageException e) {
//            return arg;
//        }
//    }

}
