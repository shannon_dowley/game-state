package net.deckserver.api.game.event

import net.deckserver.api.game.PhaseType
import net.deckserver.api.game.RegionType
import java.util.*

data class GameCreated(val gameId: UUID, val gameName: String, val seed: Long)
data class PlayerAdded(val gameId: UUID, val gameName: String, val playerId: String, val playerName: String)
data class PlayerRemoved(val gameId: UUID, val gameName: String, val playerName: String)
data class PlayerReplaced(val gameId: UUID, val playerName: String, val newPlayerName: String)
data class DeckRegistered(val gameId: UUID, val playerName: String, val crypt: List<CardInstance>, val library: List<CardInstance>)
data class GameStarted(val gameId: UUID, val gameName: String, val playerOrder: Deque<PlayerSeating>)
data class RegionShuffled(val gameId: UUID, val playerName: String, val region: RegionType)
data class CryptCardsDrawn(val gameId: UUID, val playerName: String, val amount: Int)
data class LibraryCardsDrawn(val gameId: UUID, val playerName: String, val amount: Int)
data class PoolAdjusted(val gameId: UUID, val values: List<PlayerAmount>)
data class PointsAdjusted(val gameId: UUID, val playerName: String, val amount: Float)
data class TransfersAdjusted(val gameId: UUID, val amount: Int)
data class BloodAdded(val gameId: UUID, val playerName: String, val card: String, val amount: Int)
data class BloodRemoved(val gameId: UUID, val playerName: String, val card: String, val amount: Int)
data class CardMovedToRegion(val gameId: UUID, val card: String, val playerName: String, val region: RegionType, val top: Boolean, val faceDown: Boolean)
data class CardAttachedToCard(val gameId: UUID, val card: String, val target: String, val top: Boolean, val faceDown: Boolean)
data class CardBurned(val gameId: UUID, val cards: List<String>)
data class CardsLocked(val gameId: UUID, val cards: List<String>)
data class CardsUnlocked(val gameId: UUID, val cards: List<String>)
data class PlayerOusted(val gameId: UUID, val playerName: String, val predatorName: String, val preyName: String)
data class TurnStarted(val gameId: UUID, val playerName: String)
data class PhaseStarted(val gameId: UUID, val phase: PhaseType)
data class PhaseEnded(val gameId: UUID, val phase: PhaseType)
data class PlayerAmount(val playerName: String, val amount: Int)

class PlayerSeating(val playerId: String, var playerName: String) {
    var predatorId: String? = null
    var preyId: String? = null
}

class CardInstance(val id: String) {
    var name: String = ""
    var cardId: String = ""
    var owner: String? = null
    var group: Int? = null
    var capacity: Int? = null;
    var disciplines: List<String>? = ArrayList()
}