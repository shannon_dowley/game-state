package net.deckserver.api.game.aggregate;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import net.deckserver.api.game.RegionType;

import java.util.Objects;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@JsonIdentityReference
class Player {

    private String id;
    private String name;
    private int pool;
    private float victoryPoints;
    private RegionMap regions;
    @JsonIdentityReference(alwaysAsId = true)
    private Player prey;
    @JsonIdentityReference(alwaysAsId = true)
    private Player predator;

    private boolean ousted = false;

    public Player() {}

    public Player(String id, String name) {
        this.id = id;
        this.name = name;
        this.regions = new RegionMap(this);
    }

    public Region getRegion(RegionType regionType) {
        return this.regions.get(RegionDefinition.byType(regionType));
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPool() {
        return pool;
    }

    public void setPool(int pool) {
        this.pool = pool;
    }

    public float getVictoryPoints() {
        return victoryPoints;
    }

    public void setVictoryPoints(float victoryPoints) {
        this.victoryPoints = victoryPoints;
    }

    public RegionMap getRegions() {
        return regions;
    }

    public Player getPrey() {
        return prey;
    }

    public void setPrey(Player prey) {
        this.prey = prey;
    }

    public Player getPredator() {
        return predator;
    }

    public void setPredator(Player predator) {
        this.predator = predator;
    }

    public void setOusted(boolean ousted) { this.ousted = ousted; }

    public boolean isOusted() { return this.ousted; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return getId().equals(player.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public String toString() {
        return "Player{" +
                "id='" + id + "', name='" + name + "'}";
    }
}
