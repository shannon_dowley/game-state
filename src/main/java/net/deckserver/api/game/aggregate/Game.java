package net.deckserver.api.game.aggregate;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.azam.ulidj.ULID;
import net.deckserver.api.card.CardDetail;
import net.deckserver.api.card.CardNotFoundException;
import net.deckserver.api.card.CardService;
import net.deckserver.api.game.PhaseType;
import net.deckserver.api.game.RegionType;
import net.deckserver.api.game.command.*;
import net.deckserver.api.game.event.*;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Aggregate
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Game {

    private static final int MIN_PLAYERS = 1;
    private static final int MAX_PLAYERS = 5;
    private static final int INITIAL_HAND_SIZE = 7;
    private static final int INITIAL_UNCONTROLLED_SIZE = 4;
    private static final int INITIAL_POOL = 30;
    private static final float INITIAL_VICTORY_POINTS = 0.0f;
    private static final int MIN_CRYPT_SIZE = 12;
    private static final int MIN_LIBRARY_SIZE = 60;
    private static final int MAX_LIBRARY_SIZE = 90;
    private static final int MAX_TRANSFERS = 4;
    private static final int TRANSFER_OFF_COST = 2;

    @AggregateIdentifier
    UUID id;
    Long seed;
    String name;
    CardMap cardMap = new CardMap();
    PlayerMap playerMap = new PlayerMap();
    PlayerIdMap playerIdMap = new PlayerIdMap();
    ContainerMap containerMap = new ContainerMap();
    Player currentPlayer;
    Player nextPlayer;
    Integer turn = 0;
    PhaseType phase;
    GameStatus status;

    // Available transfers for influence
    Integer transfers;

    public Game() {
    }

    @CommandHandler
    public Game(CreateGame command) {
        apply(new GameCreated(command.getGameId(), command.getGameName(), new Random().nextLong()));
    }

    @CommandHandler
    public void handle(AddPlayer command) {
        checkPlayerIsNotInGame(command.getPlayerName());
        long registeredPlayerCount = this.playerMap.size();
        if (registeredPlayerCount >= MAX_PLAYERS) {
            throw new TooManyPlayersException(command.getPlayerName());
        }
        apply(new PlayerAdded(command.getGameId(), this.name, ULID.random(), command.getPlayerName()));
    }

    @CommandHandler
    public void handle(RemovePlayer command) {
        if (this.status != GameStatus.CREATED) {
            throw new GameAlreadyStartedException(command.getPlayerName());
        }
        findInvitedPlayer(command.getPlayerName());
        apply(new PlayerRemoved(command.getGameId(), this.name, command.getPlayerName()));
    }

    @CommandHandler
    public void handle(ReplacePlayer command) {
        checkPlayerIsNotInGame(command.getNewPlayerName());
        checkPlayerExists(command.getPlayerName());
        apply(new PlayerReplaced(command.getGameId(), command.getPlayerName(), command.getNewPlayerName()));
    }

    @CommandHandler
    public void handle(RegisterDeck command, CardService cardService) {
        String playerId = findInvitedPlayer(command.getPlayerName());
        List<DeckError> errors = new ArrayList<>();
        List<CardInstance> cryptCards = new ArrayList<>();
        command.getDeckList().getCrypt().forEach(entry -> generateInstances(playerId, cardService, entry, errors, cryptCards));
        // validate crypt size
        if (MIN_CRYPT_SIZE > cryptCards.size()) {
            errors.add(new CryptTooSmallError(cryptCards.size()));
        }
        // validate crypt grouping
        Set<Integer> groups = cryptCards.stream().map(CardInstance::getGroup).collect(Collectors.toSet());
        groups.remove(null);
        Integer[] groupsArray = groups.toArray(new Integer[0]);
        if (groupsArray.length > 2) {
            errors.add(new CryptGroupingError(groups));
        } else if (groupsArray.length == 2) {
            if (Math.abs(groupsArray[0] - groupsArray[1]) > 1) {
                errors.add(new CryptGroupingError(groups));
            }
        }
        List<CardInstance> libraryCards = new ArrayList<>();
        command.getDeckList().getLibrary().forEach(entry -> generateInstances(playerId, cardService, entry, errors, libraryCards));
        // validate library size
        if (MIN_LIBRARY_SIZE > libraryCards.size()) {
            errors.add(new LibraryTooSmallError(libraryCards.size()));
        } else if (MAX_LIBRARY_SIZE < libraryCards.size()) {
            errors.add(new LibraryTooLargeError(libraryCards.size()));
        }
        // report errors
        if (errors.size() > 0) {
            throw new IllegalDeckException(command.getPlayerName(), errors);
        }
        apply(new DeckRegistered(command.getGameId(), command.getPlayerName(), cryptCards, libraryCards));
    }

    @CommandHandler
    public void handle(StartGame command) {
        if (this.playerMap.size() < MIN_PLAYERS) {
            throw new NotEnoughPlayersException(this.name);
        }
        List<String> registeredPlayers = this.playerMap.values().stream().map(Player::getName).toList();
        // Check to make sure all requested players are registered
        if (!new HashSet<>(registeredPlayers).containsAll(command.getPlayerOrder())) {
            List<String> missingPlayers = command.getPlayerOrder().stream().filter(element -> !registeredPlayers.contains(element)).sorted().collect(Collectors.toList());
            throw new IllegalPlayerOrderException(missingPlayers);
        }
        // Check to make sure all registered players are accounted for
        if (!new HashSet<>(command.getPlayerOrder()).containsAll(registeredPlayers)) {
            List<String> missingPlayers = registeredPlayers.stream().filter(element -> !command.getPlayerOrder().contains(element)).sorted().collect(Collectors.toList());
            throw new NotAllRegisteredPlayersOrderedException(missingPlayers);
        }
        LinkedList<String> players = new LinkedList<>(command.getPlayerOrder());
        Deque<PlayerSeating> seating = buildSeating(players);
        for (PlayerSeating player : seating) {
            apply(new RegionShuffled(command.getGameId(), player.getPlayerName(), RegionType.CRYPT));
            apply(new RegionShuffled(command.getGameId(), player.getPlayerName(), RegionType.LIBRARY));
            apply(new CryptCardsDrawn(command.getGameId(), player.getPlayerName(), INITIAL_UNCONTROLLED_SIZE));
            apply(new LibraryCardsDrawn(command.getGameId(), player.getPlayerName(), INITIAL_HAND_SIZE));
        }
        apply(new GameStarted(command.getGameId(), this.name, seating));
        apply(new TurnStarted(command.getGameId(), seating.getFirst().getPlayerName()));
    }

    @CommandHandler
    public void handle(EndPhase command) {
        apply(new PhaseEnded(command.getGameId(), this.phase));
        if (phase == PhaseType.DISCARD || currentPlayer.isOusted()) {
            apply(new TurnStarted(command.getGameId(), this.nextPlayer.getName()));
        } else {
            apply(new PhaseStarted(command.getGameId(), phase.next()));
        }
    }

    @CommandHandler
    public void handle(TransferOntoCrypt command) {
        Player player = findPlayer(command.getTarget().getPlayerName());
        checkPhase(player, PhaseType.INFLUENCE);
        checkOusted(player);
        // Transfer on is 1:1 ratio
        int amount = command.getTarget().getAmount();
        checkTransfers(player, amount, this.transfers);
        if (player.getPool() - amount <= 0) {
            apply(new PoolAdjusted(command.getGameId(), Collections.singletonList(new PlayerAmount(player.getName(), -amount))));
        } else {
            GameCard card = find(player.getName(), player.getRegion(RegionType.UNCONTROLLED), command.getTarget().getPosition());
            apply(new BloodAdded(command.getGameId(), command.getTarget().getPlayerName(), card.getId(), amount));
            apply(new PoolAdjusted(command.getGameId(), Collections.singletonList(new PlayerAmount(command.getTarget().getPlayerName(), -amount))));
            apply(new TransfersAdjusted(command.getGameId(), -amount));
        }

    }

    @CommandHandler
    public void handle(TransferOffCrypt command) {
        Player player = findPlayer(command.getTarget().getPlayerName());
        checkPhase(player, PhaseType.INFLUENCE);
        checkOusted(player);
        // Transfer off is 2:1 ratio
        int amount = command.getTarget().getAmount() * TRANSFER_OFF_COST;
        checkTransfers(player, amount, this.transfers);
        GameCard card = find(player.getName(), player.getRegion(RegionType.UNCONTROLLED), command.getTarget().getPosition());
        checkBlood(card, command.getTarget().getAmount());
        apply(new BloodRemoved(command.getGameId(), command.getTarget().getPlayerName(), card.getId(), command.getTarget().getAmount()));
        apply(new PoolAdjusted(command.getGameId(), Collections.singletonList(new PlayerAmount(command.getTarget().getPlayerName(), command.getTarget().getAmount()))));
        apply(new TransfersAdjusted(command.getGameId(), -amount));
    }

    @CommandHandler
    public void handle(ShuffleCrypt command) {
        Player player = findPlayer(command.getPlayerName());
        checkOusted(player);
        apply(new RegionShuffled(command.getGameId(), command.getPlayerName(), RegionType.CRYPT));
    }

    @CommandHandler
    public void handle(ShuffleLibrary command) {
        Player player = findPlayer(command.getPlayerName());
        checkOusted(player);
        apply(new RegionShuffled(command.getGameId(), command.getPlayerName(), RegionType.LIBRARY));
    }

    @CommandHandler
    public void handle(DrawLibraryCard command) {
        Player player = findPlayer(command.getPlayerName());
        checkOusted(player);
        if (player.getRegion(RegionType.LIBRARY).count() < command.getAmount()) {
            throw new UnableToDrawException(command.getPlayerName(), command.getAmount(), RegionType.LIBRARY);
        }
        apply(new LibraryCardsDrawn(command.getGameId(), command.getPlayerName(), command.getAmount()));
    }

    @CommandHandler
    public void handle(DrawCryptCard command) {
        Player player = findPlayer(command.getPlayerName());
        checkOusted(player);
        if (player.getRegion(RegionType.CRYPT).count() < command.getAmount()) {
            throw new UnableToDrawException(command.getPlayerName(), command.getAmount(), RegionType.CRYPT);
        }
        apply(new CryptCardsDrawn(command.getGameId(), command.getPlayerName(), command.getAmount()));
    }

    @CommandHandler
    public void handle(MoveCard command) {
        Player sourcePlayer = findPlayer(command.getSource().getPlayerName());
        checkOusted(sourcePlayer);
        Region sourceRegion = sourcePlayer.getRegion(command.getSource().getRegion());
        GameCard card = find(sourcePlayer.getName(), sourceRegion, command.getSource().getPosition());

        apply(new CardMovedToRegion(command.getGameId(), card.getId(), command.getTarget().getPlayerName(), command.getTarget().getRegion(), command.getTop(), command.getFaceDown()));
    }

    @CommandHandler
    public void handle(AttachCard command) {
        Player sourcePlayer = findPlayer(command.getSource().getPlayerName());
        checkOusted(sourcePlayer);
        Region sourceRegion = sourcePlayer.getRegion(command.getSource().getRegion());
        GameCard card = find(sourcePlayer.getName(), sourceRegion, command.getSource().getPosition());

        Player targetPlayer = findPlayer(command.getTarget().getPlayerName());
        checkOusted(targetPlayer);
        Region targetRegion = targetPlayer.getRegion(command.getTarget().getRegion());
        GameCard targetCard = find(targetPlayer.getName(), targetRegion, command.getTarget().getPosition());

        apply(new CardAttachedToCard(command.getGameId(), card.getId(), targetCard.getId(), command.getTop(), command.getFaceDown()));
    }

    @CommandHandler
    public void handle(BurnCard command) {
        Player sourcePlayer = findPlayer(command.getTarget().getPlayerName());
        checkOusted(sourcePlayer);
        Region sourceRegion = sourcePlayer.getRegion(command.getTarget().getRegion());
        GameCard card = find(sourcePlayer.getName(), sourceRegion, command.getTarget().getPosition());

        // BUrn this card, and all attached cards - provided they aren't already in ash heap
        if (!sourcePlayer.getRegion(RegionType.ASH_HEAP).cards().contains(card)) {
            List<String> cardIds = card.cards().stream().map(GameCard::getId).collect(Collectors.toList());
            cardIds.add(card.getId());
            apply(new CardBurned(command.getGameId(), cardIds));
        } else {
            throw new InvalidCardTargetException(command.getTarget().getPlayerName(), command.getTarget().getRegion(), command.getTarget().getPosition());
        }
    }

    @CommandHandler
    public void handle(LockCard command) {
        Player sourcePlayer = findPlayer(command.getTarget().getPlayerName());
        checkOusted(sourcePlayer);
        Region sourceRegion = sourcePlayer.getRegion(command.getTarget().getRegion());
        GameCard card = find(sourcePlayer.getName(), sourceRegion, command.getTarget().getPosition());

        if (!card.isLocked()) {
            apply(new CardsLocked(command.getGameId(), List.of(card.getId())));
        }
    }

    @CommandHandler
    public void handle(UnlockCard command) {
        Player sourcePlayer = findPlayer(command.getTarget().getPlayerName());
        checkOusted(sourcePlayer);
        Region sourceRegion = sourcePlayer.getRegion(command.getTarget().getRegion());
        GameCard card = find(sourcePlayer.getName(), sourceRegion, command.getTarget().getPosition());

        if (card.isLocked()) {

            apply(new CardsUnlocked(command.getGameId(), List.of(card.getId())));
        }
    }

    @CommandHandler
    public void handle(UnlockAll command) {
        Player sourcePlayer = findPlayer(command.getPlayerName());
        checkOusted(sourcePlayer);
        checkPhase(sourcePlayer, PhaseType.UNLOCK);
        List<String> cards = sourcePlayer.getRegion(RegionType.IN_PLAY).cards().stream()
                .filter(card -> card.getOwner().equals(sourcePlayer.getId()))
                .filter(card -> !card.hasFeature(FeatureType.DOES_NOT_UNLOCK_AS_NORMAL))
                .map(GameCard::getId).toList();
        apply(new CardsUnlocked(command.getGameId(), cards));
    }

    @CommandHandler
    public void handle(AdjustPool command) {
        List<PlayerAmount> playerAmounts = new ArrayList<>();
        command.getTargets()
                .forEach(adjustment -> {
                    Player player = findPlayer(adjustment.getPlayerName());
                    checkOusted(player);
                    playerAmounts.add(new PlayerAmount(adjustment.getPlayerName(), adjustment.getAmount()));
                });
        apply(new PoolAdjusted(command.getGameId(), playerAmounts));
    }

    @EventSourcingHandler
    public void on(GameCreated event) {
        this.id = event.getGameId();
        this.name = event.getGameName();
        this.status = GameStatus.CREATED;
        this.seed = event.getSeed();
    }

    @EventSourcingHandler
    public void on(PlayerAdded event) {
        String playerId = event.getPlayerId();
        this.playerIdMap.put(event.getPlayerName(), playerId);
    }

    @EventSourcingHandler
    public void on(PlayerReplaced event) {
        Player player = findPlayer(event.getPlayerName());
        player.setName(event.getNewPlayerName());
        String playerId = player.getId();
        this.playerIdMap.remove(event.getPlayerName());
        this.playerIdMap.put(event.getNewPlayerName(), playerId);
    }

    @EventSourcingHandler
    public void on(PlayerRemoved event) {
        String playerId = this.playerIdMap.remove(event.getPlayerName());
        this.playerMap.remove(playerId);
    }

    @EventSourcingHandler
    public void on(DeckRegistered event) {
        String playerId = this.playerIdMap.get(event.getPlayerName());
        Player player = new Player(playerId, event.getPlayerName());
        List<GameCard> crypt = event.getCrypt().stream().map(cardInstance -> mapToCard(cardInstance, player)).collect(Collectors.toList());
        List<GameCard> library = event.getLibrary().stream().map(cardInstance -> mapToCard(cardInstance, player)).collect(Collectors.toList());
        player.getRegion(RegionType.CRYPT).populate(crypt);
        player.getRegion(RegionType.LIBRARY).populate(library);
        Stream.concat(crypt.stream(), library.stream()).forEach(gameCard -> this.cardMap.put(gameCard.getId(), gameCard));
        player.getRegions().values().forEach(region -> this.containerMap.put(region.getId(), region));
        this.containerMap.putAll(this.cardMap);
        this.playerMap.put(playerId, player);
    }

    @EventSourcingHandler
    public void on(GameStarted event) {
        this.status = GameStatus.STARTED;
        event.getPlayerOrder().forEach(seating -> {
            Player player = findPlayer(seating.getPlayerName());
            Player predator = this.playerMap.get(seating.getPredatorId());
            Player prey = this.playerMap.get(seating.getPreyId());
            player.setPredator(predator);
            player.setPrey(prey);
            player.setPool(INITIAL_POOL);
            player.setVictoryPoints(INITIAL_VICTORY_POINTS);
        });
        this.phase = PhaseType.UNLOCK;
    }

    @EventSourcingHandler
    public void on(RegionShuffled event) {
        Player player = findPlayer(event.getPlayerName());
        Region region = player.getRegion(event.getRegion());
        Random random = new Random(this.seed);
        this.seed = random.nextLong();
        region.shuffle(random);
    }

    @EventSourcingHandler
    public void on(CryptCardsDrawn event) {
        Player player = findPlayer(event.getPlayerName());
        Region sourceRegion = player.getRegion(RegionType.CRYPT);
        Region targetRegion = player.getRegion(RegionType.UNCONTROLLED);
        for (int x = 0; x < event.getAmount(); x++) {
            GameCard gameCard = sourceRegion.getFirstCard();
            gameCard.faceDown();
            move(gameCard, targetRegion);
        }
    }

    @EventSourcingHandler
    public void on(LibraryCardsDrawn event) {
        Player player = findPlayer(event.getPlayerName());
        Region sourceRegion = player.getRegion(RegionType.LIBRARY);
        Region targetRegion = player.getRegion(RegionType.HAND);
        for (int x = 0; x < event.getAmount(); x++) {
            GameCard gameCard = sourceRegion.getFirstCard();
            gameCard.faceDown();
            move(gameCard, targetRegion);
        }
    }

    @EventSourcingHandler
    public void on(CardMovedToRegion event) {
        GameCard gameCard = this.cardMap.get(event.getCard());
        gameCard.setFacing(event.getFaceDown());
        CardContainer target = findPlayer(event.getPlayerName()).getRegion(event.getRegion());
        move(gameCard, target, event.getTop());
    }

    @EventSourcingHandler
    public void on(CardAttachedToCard event) {
        GameCard gameCard = this.cardMap.get(event.getCard());
        gameCard.setFacing(event.getFaceDown());
        GameCard target = this.cardMap.get(event.getTarget());
        move(gameCard, target, event.getTop());
    }

    @EventSourcingHandler
    public void on(CardsLocked event) {
        for (String id : event.getCards()) {
            GameCard gameCard = this.cardMap.get(id);
            gameCard.lock();
        }
    }

    @EventSourcingHandler
    public void on(CardBurned event) {
        for (String id : event.getCards()) {
            GameCard gameCard = this.cardMap.get(id);
            Player owner = this.playerMap.get(gameCard.getOwner());
            gameCard.unlock();
            gameCard.faceUp();
            gameCard.clearCounters();
            move(gameCard, owner.getRegion(RegionType.ASH_HEAP));
        }
    }

    @EventSourcingHandler
    public void on(CardsUnlocked event) {
        for (String id : event.getCards()) {
            GameCard gameCard = this.cardMap.get(id);
            gameCard.unlock();
        }
    }

    @EventSourcingHandler
    public void on(PoolAdjusted event) {
        List<Player> oustedPlayers = new ArrayList<>();
        List<Player> predators = new ArrayList<>();
        event.getValues().forEach(amount ->
        {
            Player player = findPlayer(amount.getPlayerName());
            Player predator = player.getPredator();
            int currentPool = player.getPool();
            int newPool = currentPool + amount.getAmount();
            player.setPool(newPool);
            if (player.getPool() <= 0) {
                oustedPlayers.add(player);
                apply(new PointsAdjusted(event.getGameId(), predator.getName(), 1.0f));
                predators.add(predator);
            }
        });

        for (Player o : oustedPlayers) {
            Player predator = o.getPredator();
            Player prey = o.getPrey();
            apply(new PlayerOusted(event.getGameId(), o.getName(), predator.getName(), prey.getName()));
            predators.remove(o);
        }
        predators.forEach(predator -> apply(new PoolAdjusted(event.getGameId(), Collections.singletonList(new PlayerAmount(predator.getName(), 6)))));
    }

    @EventSourcingHandler
    public void on(PointsAdjusted event) {
        Player player = findPlayer(event.getPlayerName());
        float currentPoints = player.getVictoryPoints();
        float newPoints = currentPoints + event.getAmount();
        player.setVictoryPoints(newPoints);
    }

    @EventSourcingHandler
    public void on(TransfersAdjusted event) {
        this.transfers += event.getAmount();
    }

    @EventSourcingHandler
    public void on(PlayerOusted event) {
        Player player = findPlayer(event.getPlayerName());
        Player predator = findPlayer(event.getPredatorName());
        Player prey = findPlayer(event.getPreyName());
        if (this.nextPlayer.equals(player)) {
            this.nextPlayer = player.getPrey();
        }
        predator.setPrey(prey);
        prey.setPredator(predator);
        player.setPredator(null);
        player.setPrey(null);
        player.setOusted(true);
    }

    @EventSourcingHandler
    public void on(TurnStarted event) {
        Player player = findPlayer(event.getPlayerName());
        this.currentPlayer = player;
        this.nextPlayer = player.getPrey();
        this.turn++;
        this.phase = PhaseType.UNLOCK;
    }

    @EventSourcingHandler
    public void on(PhaseEnded event) {
        if (event.getPhase() == PhaseType.INFLUENCE) {
            transfers = 0;
        }
    }

    @EventSourcingHandler
    public void on(PhaseStarted event) {
        this.phase = event.getPhase();
        if (this.phase == PhaseType.INFLUENCE) {
            transfers = Math.min(turn, MAX_TRANSFERS);
        }
    }

    @EventSourcingHandler
    public void on(BloodAdded event) {
        GameCard card = this.cardMap.get(event.getCard());
        card.addCounters(CounterType.BLOOD, event.getAmount());
    }

    @EventSourcingHandler
    public void on(BloodRemoved event) {
        GameCard card = this.cardMap.get(event.getCard());
        card.removeCounters(CounterType.BLOOD, event.getAmount());
    }

    Player findPlayer(String playerName) {
        String playerId = this.playerIdMap.get(playerName);
        if (playerId == null) {
            throw new PlayerNotFoundException(playerName);
        }
        return this.playerMap.get(playerId);
    }

    private String findInvitedPlayer(String playerName) {
        if (!this.playerIdMap.containsKey(playerName)) {
            throw new PlayerNotInvitedException(playerName);
        }
        return this.playerIdMap.get(playerName);
    }

    private void checkPhase(Player player, PhaseType type) {
        if (this.phase != type || this.currentPlayer != player) {
            throw new InvalidPhaseException(player.getName(), type, this.currentPlayer.getName(), this.phase);
        }
    }

    private void checkTransfers(Player player, int request, int available) {
        if (request > available) {
            throw new NotEnoughTransfersException(player.getName(), request, available);
        }
    }

    private void checkBlood(GameCard card, int amount) {
        if (card.getCount(CounterType.BLOOD) < amount) {
            throw new NotEnoughBloodException(this.playerMap.get(card.getOwner()).getName(), card.getName(), card.getCount(CounterType.BLOOD), amount);
        }
    }

    private void checkPlayerIsNotInGame(String playerName) {
        if (playerIdMap.containsKey(playerName)) {
            throw new PlayerAlreadyAddedException(playerName);
        }

    }

    private void checkPlayerExists(String playerName) {
        if (!this.playerIdMap.containsKey(playerName)) {
            throw new PlayerNotFoundException(playerName);
        }
    }

    private void checkOusted(Player player) {
        if (player.isOusted()) {
            throw new PlayerOustedException(player.getName());
        }
    }

    private void move(GameCard gameCard, CardContainer target) {
        move(gameCard, target, false);
    }

    private void move(GameCard gameCard, CardContainer target, boolean top) {
        target.addCard(gameCard, top);
    }

    private static void generateInstances(String playerId, CardService cardService, CardEntry entry, List<DeckError> errors, List<CardInstance> gameCards) {
        try {
            CardDetail card = cardService.getCard(entry.getId());
            if (card.getBanned()) {
                errors.add(new CardBannedError(card.getName()));
            } else {
                for (int x = 0; x < entry.getCount(); x++) {
                    CardInstance instance = new CardInstance(ULID.random());
                    instance.setName(card.getName());
                    instance.setOwner(playerId);
                    instance.setCardId(card.getId());
                    instance.setCapacity(card.getCapacity());
                    instance.setDisciplines(card.getDisciplines());
                    Optional.ofNullable(card.getGroup()).ifPresent(instance::setGroup);
                    gameCards.add(instance);
                }
            }
        } catch (CardNotFoundException e) {
            errors.add(new CardNotFoundError(entry.getId()));
        }
    }

    private Deque<PlayerSeating> buildSeating(LinkedList<String> players) {
        Deque<PlayerSeating> seating = new ArrayDeque<>();
        PlayerSeating current;
        PlayerSeating first = null;
        PlayerSeating predator = null;
        for (String player : players) {
            current = new PlayerSeating(this.playerIdMap.get(player), player);
            if (first == null) {
                first = current;
            }
            seating.add(current);
            if (predator != null) {
                current.setPredatorId(predator.getPlayerId());
                predator.setPreyId(current.getPlayerId());
            }
            predator = current;
            if (player.equals(players.getLast())) {
                current.setPreyId(first.getPlayerId());
                first.setPredatorId(current.getPlayerId());
            }
        }
        return seating;
    }

    private static GameCard mapToCard(CardInstance cardInstance, Player player) {
        GameCard gameCard = new GameCard();
        gameCard.setId(cardInstance.getId());
        gameCard.setCardId(cardInstance.getCardId());
        gameCard.setName(cardInstance.getName());
        gameCard.setDisciplines(Optional.ofNullable(cardInstance.getDisciplines()).stream().flatMap(List::stream).map(Discipline::new).collect(Collectors.toSet()));
        gameCard.setCapacity(cardInstance.getCapacity());
        gameCard.setOwner(player.getId());
        return gameCard;
    }

    GameCard find(String playerName, Region parent, List<Integer> position) {
        CardContainer cardContainer = parent;
        GameCard result = null;
        try {
            for (int x : position) {
                cardContainer = cardContainer.getCard(x);
                result = (GameCard) cardContainer;
            }
            return result;
        } catch (IndexOutOfBoundsException e) {
            throw new InvalidCardTargetException(playerName, parent.getDefinition().getType(), position);
        }
    }

}
