package net.deckserver.api.game.aggregate

import net.deckserver.api.game.DisciplineType
import net.deckserver.api.game.RegionType
import java.util.*

internal class RegionMap() : TreeMap<RegionDefinition, Region>() {
    constructor(player: Player) : this() {
        for (definition in RegionDefinition.values()) {
            put(definition, Region(player, definition))
        }
    }
}

internal class CardMap : TreeMap<String, GameCard>()
internal class PlayerMap : TreeMap<String, Player>()
internal class PlayerIdMap : TreeMap<String, String>()
internal class ContainerMap : TreeMap<String, CardContainer>()

internal enum class VisibilityType { VISIBLE, HIDDEN }

internal enum class AccessControlType { RESTRICTED, PUBLIC }

internal enum class GameStatus { CREATED, STARTED, FINISHED }

internal enum class FeatureType { DOES_NOT_UNLOCK_AS_NORMAL, NOT_UNIQUE }

//internal enum class EffectType { EXTRA_TRANSFERS, BLEED_MODIFIER, STRENGTH_MODIFIER }

internal enum class CounterType { BLOOD, CORRUPTION, LIFE, CUSTOM }

internal enum class CardType { NONE, VAMPIRE, IMBUED, ALLY, RETAINER, EQUIPMENT, LOCATION}

internal enum class DisciplineLevel { BASIC, SUPERIOR }

internal enum class MinionState { READY, TORPOR, INCAPACITATED}

internal enum class RegionDefinition(
    val type: RegionType,
    val ownerVisibility: VisibilityType,
    val otherVisibilityType: VisibilityType,
    val accessControlType: AccessControlType
) {
    IN_PLAY(RegionType.IN_PLAY, VisibilityType.VISIBLE, VisibilityType.VISIBLE, AccessControlType.PUBLIC),
    OUT_OF_PLAY(RegionType.OUT_OF_PLAY, VisibilityType.VISIBLE, VisibilityType.VISIBLE, AccessControlType.PUBLIC),
    ASH_HEAP(RegionType.ASH_HEAP, VisibilityType.VISIBLE, VisibilityType.VISIBLE, AccessControlType.PUBLIC),
    REMOVED_FROM_GAME(RegionType.REMOVED_FROM_GAME, VisibilityType.VISIBLE, VisibilityType.VISIBLE, AccessControlType.PUBLIC),
    CRYPT(RegionType.CRYPT, VisibilityType.HIDDEN, VisibilityType.HIDDEN, AccessControlType.RESTRICTED),
    LIBRARY(RegionType.LIBRARY, VisibilityType.HIDDEN, VisibilityType.HIDDEN, AccessControlType.RESTRICTED),
    UNCONTROLLED(RegionType.UNCONTROLLED, VisibilityType.VISIBLE, VisibilityType.HIDDEN, AccessControlType.RESTRICTED),
    HAND(RegionType.HAND, VisibilityType.VISIBLE, VisibilityType.HIDDEN, AccessControlType.RESTRICTED),
    RESEARCH(RegionType.RESEARCH, VisibilityType.VISIBLE, VisibilityType.HIDDEN, AccessControlType.RESTRICTED);

    companion object {
        @JvmStatic
        fun byType(type: RegionType?): RegionDefinition {
            for (definition: RegionDefinition in values()) {
                if (definition.type == type) {
                    return definition
                }
            }
            throw IllegalArgumentException("Definition for $type not found")
        }
    }
}

internal class Discipline(var type: DisciplineType, var level: DisciplineLevel) {
    constructor(code: String) : this(
        DisciplineType.byCode(code),
        if (DisciplineType.byCode(code).code.uppercase() == code) DisciplineLevel.SUPERIOR else DisciplineLevel.BASIC
    )

    override fun toString(): String {
        return if (level == DisciplineLevel.SUPERIOR) type.code.uppercase() else type.code
    }


}
