package net.deckserver.api.game.aggregate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import net.deckserver.api.game.DisciplineType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
class GameCard extends CardContainer {

    private String owner;
    private String cardId;
    private String name;
    private boolean faceDown = true;
    private boolean locked = false;
    private MinionState state = null;
    private CardType type = CardType.NONE;
    private Integer capacity;
    private Map<CounterType, Integer> counters = new HashMap<>();
    private Set<FeatureType> features = new HashSet<>();
    private Set<Discipline> disciplines = new HashSet<>();

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFacing(boolean facing) {
        this.faceDown = facing;
    }

    public boolean isFaceDown() {
        return faceDown;
    }

    public boolean isLocked() {
        return locked;
    }

    public void faceDown() {
        this.faceDown = true;
    }

    public void faceUp() {
        this.faceDown = false;
    }

    public void lock() {
        this.locked = true;
    }

    public void unlock() {
        this.locked = false;
    }

    public boolean isReady() { return this.state == MinionState.READY; }

    public CardType getType() {return this.type;}

    @JsonIgnore
    public boolean hasFeature(FeatureType type) {
        return this.features.contains(type);
    }

    @JsonIgnore
    public int getCount(CounterType type) {
        return this.counters.getOrDefault(type, 0);
    }

    public void addCounters(CounterType type, int amount) {
        int current = getCount(type);
        this.counters.put(type, current + amount);
    }

    public void removeCounters(CounterType type, int amount) {
        int current = getCount(type);
        this.counters.put(type, Math.max(current - amount, 0));
    }

    public void clearCounters() {
        this.counters = new HashMap<>();
    }

    public void setDisciplines(Set<Discipline> disciplines) {
        this.disciplines = disciplines;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "GameCard{" +
                "name='" + name + "', " +
                "id='" + getId() + "'}";
    }
}
