package net.deckserver.api.game.aggregate;

class Region extends CardContainer {

    private RegionDefinition definition;

    public Region() {
    }

    public Region(Player player, RegionDefinition definition) {
        this.definition = definition;
        this.setId(player.getId() + "-" + definition.getType());
    }

    public RegionDefinition getDefinition() {
        return definition;
    }

    @Override
    public String toString() {
        return "Region{" +
                "type='" + definition.getType() + "'}";
    }
}
