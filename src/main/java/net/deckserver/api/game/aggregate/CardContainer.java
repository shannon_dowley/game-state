package net.deckserver.api.game.aggregate;

import com.fasterxml.jackson.annotation.*;

import java.util.*;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "@class")
@JsonIdentityReference
@JsonInclude(JsonInclude.Include.NON_EMPTY)
abstract class CardContainer {

    @JsonIdentityReference(alwaysAsId = true)
    private LinkedList<GameCard> gameCards = new LinkedList<>();

    @JsonIdentityReference(alwaysAsId = true)
    private CardContainer parent;

    private String id;

    public int count() {
        int count = this.gameCards.size();
        for (var gameCard : gameCards) {
            count += gameCard.count();
        }
        return count;
    }

    List<GameCard> cards() {
        var cards = new ArrayList<>(this.gameCards);
        for (var gameCard : gameCards) {
            cards.addAll(gameCard.cards());
        }
        return cards;
    }

    public void populate(List<GameCard> gameCards) {
        this.gameCards = new LinkedList<>(gameCards);
        this.gameCards.forEach(c -> c.setParent(this));
    }

    public void shuffle(Random random) {
        List<GameCard> gameCards = new LinkedList<>(this.gameCards);
        Collections.shuffle(gameCards, random);
        this.gameCards = new LinkedList<>(gameCards);
    }

    public void removeCard(GameCard gameCard) {
        this.gameCards.remove(gameCard);
        gameCard.setParent(null);
    }

    public void addCard(GameCard gameCard, boolean top) {
        Optional.ofNullable(gameCard.getParent()).ifPresent(parent -> parent.removeCard(gameCard));
        gameCard.setParent(this);
        if (top) {
            gameCards.addFirst(gameCard);
        } else {
            gameCards.addLast(gameCard);
        }
    }

    @JsonIgnore
    public GameCard getCard(int position) {
        return this.gameCards.get(position - 1);
    }

    @JsonIgnore
    public GameCard getFirstCard() {
        return this.gameCards.getFirst();
    }

    public CardContainer getParent() {
        return parent;
    }

    public String getId() {
        return id;
    }

    public void setParent(CardContainer parent) {
        this.parent = parent;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CardContainer)) return false;
        CardContainer that = (CardContainer) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

}
