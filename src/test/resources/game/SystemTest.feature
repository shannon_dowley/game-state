Feature: Test global commands

  Scenario: Creating a game
    Given there is no game
    When a game called "Test game" is created
    Then the game name will be "Test game"

  Scenario: Creating a game
    Given there is no game
    When a game called "Test game" is created
    And a player called Player 1 is added
    And a player called Player 2 is added
    And a player called Player 3 is added
    And a player called Player 4 is added
    And a player called Player 5 is added
    And Player 1 has registered a valid deck
    And Player 2 has registered a valid deck
    And Player 3 has registered a valid deck
    And Player 4 has registered a valid deck
    And Player 5 has registered a valid deck
    And the game is started
    Then these players will have the following state:
      | Name     | Pool | Victory Points |
      | Player 1 | 30   | 0              |
      | Player 2 | 30   | 0              |
      | Player 3 | 30   | 0              |
      | Player 4 | 30   | 0              |
      | Player 5 | 30   | 0              |