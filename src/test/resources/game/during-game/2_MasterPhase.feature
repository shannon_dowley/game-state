Feature: Actions in the master phase

  Background:
    Given this is an existing game

  Scenario: End the phase
    Given the turn has progressed to the Master phase
    When the current phase is ended
    Then the current player will be Player 1
    And the current phase will be the Minion phase

  Scenario: An ousted player should be able to end the phase
    Given the following pool adjustments are made:
      | Name     | Change |
      | Player 1 | -5     |
    When the current phase is ended
    Then the current player will be Player 2
    And the current phase will be the Unlock phase