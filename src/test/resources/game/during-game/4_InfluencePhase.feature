Feature: Actions in the influence phase

  Scenario: End the phase
    Given this is a new game
    And the turn has progressed to the Influence phase
    When the current phase is ended
    Then the current player will be Player 1
    And the current phase will be the Discard phase
    Then these players will have the following state:
      | Name     | Pool | Transfers |
      | Player 1 | 30   | 0         |

  Scenario: Transfer onto card
    Given this is a new game
    And the turn has progressed to the Influence phase
    When Player 1 transfers 1 blood onto card 1
    Then these cards will have the following state:
      | Name     | Region       | Card Position | Card Name | Blood | Locked |
      | Player 1 | Uncontrolled | 1             | Anson     | 1     | False  |
    Then these players will have the following state:
      | Name     | Pool | Transfers |
      | Player 1 | 29   | 0         |

  Scenario: Transfer off a card
    Given this is an existing game
    And the turn has progressed to the Influence phase
    When Player 1 transfers 1 blood from card 1
    Then these cards will have the following state:
      | Name     | Region       | Card Position | Card Name | Blood | Locked |
      | Player 1 | Uncontrolled | 1             | Anson     | 7     | False  |
    And these players will have the following state:
      | Name     | Pool | Transfers |
      | Player 1 | 6    | 2         |

  Scenario: Transfer off a card with no blood
    Given this is an existing game
    And the turn has progressed to the Influence phase
    When Player 1 transfers 1 blood from card 2
    Then there will be an error message in English: "Not enough blood on Player 1's Anneke.  Needs 1, has 0."

  Scenario: Transfer too much onto card
    Given this is a new game
    And the turn has progressed to the Influence phase
    When Player 1 transfers 2 blood onto card 1
    Then there will be an error message in English: "Unable to transfer 2 blood, only 1 transfer available for Player 1."

  Scenario: Transfer out
    Given this is an existing game
    And the turn has progressed to the Influence phase
    And the following pool adjustments are made:
      | Name     | Change |
      | Player 1 | -2     |
    When Player 1 transfers 3 blood onto card 2
    And these players will have the following state:
      | Name     | Pool | Ousted | Victory Points |
      | Player 1 | 0    | True   | 0              |
      | Player 5 | 11   | False  | 1.0            |

  Scenario: An ousted player should be able to end the phase
    Given this is an existing game
    Given the following pool adjustments are made:
      | Name     | Change |
      | Player 1 | -5     |
    When the current phase is ended
    Then the current player will be Player 2
    And the current phase will be the Unlock phase