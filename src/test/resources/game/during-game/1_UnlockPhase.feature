Feature: Actions in the unlock phase

  Background:
    Given this is an existing game

  Scenario: Cards are unlocked in Unlock phase
    Given Player 1 moves card 1 from Uncontrolled to Play region
    And Player 1 locks card 1 in Play region
    And these cards will have the following state:
      | Name     | Region      | Card Name       | Card Position | Locked | Face Down |
      | Player 1 | Play region | François Villon | 2             | True   | False     |
      | Player 1 | Play region | Anson           | 1             | True   | False     |
    When Player 1 unlocks all their cards
    Then these cards will have the following state:
      | Name     | Region      | Card Name       | Card Position | Locked | Face Down |
      | Player 1 | Play region | François Villon | 2             | True   | False     |
      | Player 1 | Play region | Anson           | 1             | False  | False     |

  Scenario: Wrong player tries to unlock
    Given the turn has progressed to the Unlock phase
    When Player 2 unlocks all their cards
    Then there will be an error message in English: "Needs to be Player 2's Unlock phase for that command.  Currently Player 1's Unlock phase."

  Scenario: Cards are unlocked in wrong phase
    Given the turn has progressed to the Minion phase
    When Player 1 unlocks all their cards
    Then there will be an error message in English: "Needs to be Player 1's Unlock phase for that command.  Currently Player 1's Minion phase."

  Scenario: End the phase
    When the current phase is ended
    Then the current player will be Player 1
    And the current phase will be the Master phase

  Scenario: An ousted player should be able to end the phase
    Given the following pool adjustments are made:
      | Name     | Change |
      | Player 1 | -5     |
    When the current phase is ended
    Then the current player will be Player 2
    And the current phase will be the Unlock phase

  Scenario: An ousted player can't unlock
    Given the following pool adjustments are made:
      | Name     | Change |
      | Player 1 | -5     |
    When Player 1 unlocks all their cards
    Then there will be an error message in English: "Invalid command.  Player 1 is ousted."