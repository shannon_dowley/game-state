Feature: Adjusting a card state

  Background:
    Given this is a new game

  Scenario: A card is locked
    Given Player 1 moves card 1 from Uncontrolled to Play region
    When Player 1 locks card 1 in Play region
    Then these cards will have the following state:
      | Name     | Region      | Card Position | Card Name | Blood | Locked | Face Down |
      | Player 1 | Play region | 1             | Anson     | 0     | True   | False     |

  Scenario: A card is locked already, no error
    Given Player 1 moves card 1 from Uncontrolled to Play region
    And Player 1 locks card 1 in Play region
    When Player 1 locks card 1 in Play region
    Then these cards will have the following state:
      | Name     | Region      | Card Position | Card Name | Blood | Locked | Face Down |
      | Player 1 | Play region | 1             | Anson     | 0     | True   | False     |

  Scenario: A card is unlocked
    Given Player 1 moves card 1 from Uncontrolled to Play region
    And Player 1 locks card 1 in Play region
    When Player 1 unlocks card 1 in Play region
    Then these cards will have the following state:
      | Name     | Region      | Card Position | Card Name | Locked | Face Down |
      | Player 1 | Play region | 1             | Anson     | False  | False     |

  Scenario: A card is unlocked already, no error
    Given Player 1 moves card 1 from Uncontrolled to Play region
    When Player 1 unlocks card 1 in Play region
    Then these cards will have the following state:
      | Name     | Region      | Card Position | Card Name | Locked | Face Down |
      | Player 1 | Play region | 1             | Anson     | False  | False     |

  Scenario: An ousted player tries to unlock a card
    Given Player 1 moves card 1 from Uncontrolled to Play region
    And the following pool adjustments are made:
      | Name     | Change |
      | Player 1 | -30    |
    When Player 1 unlocks card 1 in Play region
    Then there will be an error message in English: "Invalid command.  Player 1 is ousted."