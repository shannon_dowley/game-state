Feature: Adjusting pool and ousting

  Background:
    Given this is an existing game

  Scenario: Adjusting a player's pool
    When the following pool adjustments are made:
      | Name     | Change |
      | Player 1 | -3     |
    Then Player 1 will have 2 pool

  Scenario: Adjusting multiple player's pool
    When the following pool adjustments are made:
      | Name     | Change |
      | Player 1 | -3     |
      | Player 2 | +3     |
    Then Player 1 will have 2 pool
    Then Player 2 will have 8 pool

  Scenario: A player is ousted
    When the following pool adjustments are made:
      | Name     | Change |
      | Player 2 | -5     |
    Then these players will have the following state:
      | Name     | Pool | Victory Points |
      | Player 1 | 11   | 1.0            |
      | Player 2 | 0    | 0              |
      | Player 3 | 5    | 0              |
      | Player 4 | 5    | 0              |
      | Player 5 | 5    | 0              |

  Scenario: The current player is ousted
    When the following pool adjustments are made:
      | Name     | Change |
      | Player 1 | -5     |
    Then these players will have the following state:
      | Name     | Pool | Victory Points | Ousted |
      | Player 1 | 0    | 0              | True   |
      | Player 2 | 5    | 0              | False  |
      | Player 3 | 5    | 0              | False  |
      | Player 4 | 5    | 0              | False  |
      | Player 5 | 11   | 1.0            | False  |
    And the next player will be Player 2

  Scenario: Multiple players are ousted
    When the following pool adjustments are made:
      | Name     | Change |
      | Player 3 | -5     |
      | Player 1 | -5     |
      | Player 2 | -5     |
    Then these players will have the following state:
      | Name     | Pool | Victory Points | Ousted |
      | Player 1 | 0    | 1.0            | True   |
      | Player 2 | 0    | 1.0            | True   |
      | Player 3 | 0    | 0              | True   |
      | Player 4 | 5    | 0              | False  |
      | Player 5 | 11   | 1.0            | False  |
    And the next player will be Player 4