Feature: Shuffle various regions

  Background:
    Given this is an existing game

  Scenario: Shuffle a region
    Given these cards are in the following state:
      | Name     | Region  | Card Position | Card Name                          |
      | Player 1 | Library | 1             | Resist Earth's Grasp               |
      | Player 1 | Library | 15            | Villein                            |
      | Player 1 | Library | 30            | Parity Shift                       |
      | Player 1 | Library | 45            | Concealed Weapon                   |
      | Player 1 | Library | 60            | Liquidation                        |
      | Player 1 | Library | 75            | Codex of the Edenic Groundskeepers |
      | Player 2 | Crypt   | 1             | Masika                             |
    When Player 1 shuffles Library
    And Player 2 shuffles Crypt
    Then the Library of Player 1 will be shuffled
    And the Crypt of Player 2 will be shuffled
    Then these cards will have the following state:
      | Name     | Region  | Card Position | Card Name               |
      | Player 1 | Library | 1             | Psyche!                 |
      | Player 1 | Library | 15            | Parity Shift            |
      | Player 1 | Library | 30            | Telepathic Misdirection |
      | Player 1 | Library | 45            | Liquidation             |
      | Player 1 | Library | 60            | Entrancement            |
      | Player 1 | Library | 75            | Blur                    |
      | Player 2 | Crypt   | 1             | Anson                   |

  Scenario: Trying to shuffle a region when not in game
    When Player 6 shuffles Crypt
    Then there will be an error message in Spanish: "Player 6 no en juego o no registrado."

  Scenario: Trying to shuffle library while ousted
    Given the following pool adjustments are made:
      | Name     | Change |
      | Player 1 | -5     |
    When Player 1 shuffles Library
    Then there will be an error message in English: "Invalid command.  Player 1 is ousted."

  Scenario: Trying to shuffle crypt while ousted
    Given the following pool adjustments are made:
      | Name     | Change |
      | Player 1 | -5     |
    When Player 1 shuffles Crypt
    Then there will be an error message in English: "Invalid command.  Player 1 is ousted."