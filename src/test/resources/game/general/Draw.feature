Feature: Drawing cards

  Background:
    Given this is an existing game

  Scenario: Draw a library card
    When Player 1 draws 1 library card
    Then Player 1 will have 8 cards in Hand

  Scenario: Draw a crypt card
    When Player 1 draws 2 crypt cards
    Then Player 1 will have 6 cards in Uncontrolled

  Scenario: Draw too many crypt cards
    When Player 1 draws 9 crypt cards
    Then there will be an error message in English: "Unable to draw 9 cards from Player 1's Crypt.  Not enough cards left."
    And there will be an error message in Spanish: "No se pueden robar 9 cartas de la Cripta de Player 1.  No quedan suficientes tarjetas."

  Scenario: Draw too many library cards
    When Player 1 draws 84 library cards
    Then there will be an error message in English: "Unable to draw 84 cards from Player 1's Library.  Not enough cards left."