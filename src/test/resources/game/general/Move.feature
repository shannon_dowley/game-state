Feature: Moving and attaching cards

  Background:
    Given this is an existing game

  Scenario: A card is moved legally
    When Player 1 moves card 7 from Hand to Play region
    Then Player 1 will have 2 card in Play region
    And Player 1 will have 83 cards in Library

  Scenario: A card is targeted incorrectly
    When Player 1 moves card 7.1 from Hand to Play region
    Then there will be an error message in English: "Invalid card at Player 1's Hand -> [7, 1]"

  Scenario: A card is moved to the bottom
    Given these cards are in the following state:
      | Name     | Region  | Card Position | Card Name            |
      | Player 1 | Library | 1             | Resist Earth's Grasp |
      | Player 1 | Library | 83            | .44 Magnum           |
      | Player 1 | Hand    | 1             | Charismatic Aura     |
    When Player 1 moves card 1 from Hand to the bottom of Library
    Then Player 1 will have 84 cards in Library
    And these cards will have the following state:
      | Name     | Region  | Card Position | Card Name            |
      | Player 1 | Library | 1             | Resist Earth's Grasp |
      | Player 1 | Library | 83            | .44 Magnum           |
      | Player 1 | Library | 84            | Charismatic Aura     |

  Scenario: A card is attached to another card
    Given Player 1 moves card 1 from Hand to Play region
    When Player 1 attaches card 1 from Hand to card 1 in Player 1's Play region
    And Player 1 will have 3 cards in Play region
    And Player 1 will have card in Play region position 1 with 1 card attached

  Scenario: An attached card is moved
    Given Player 1 moves card 1 from Hand to Play region
    And Player 1 attaches card 1 from Hand to card 1 in Player 1's Play region
    When Player 1 moves card 1.1 from Play region to Ash heap
    Then Player 1 will have 2 cards in Play region
    And Player 1 will have 5 cards in Hand
    And Player 1 will have 1 cards in Ash heap

  Scenario: A card is burned
    Given Player 1 has played card 1 from Uncontrolled to Play region
    And Player 4 has attached card 7 from Hand to card 1 in Player 1's Play region
    And these cards are in the following state:
      | Name     | Region      | Card Position | Card Name             | Blood | Locked |
      | Player 1 | Play region | 1             | Anson                 | 8     | False  |
      | Player 1 | Play region | 1.1           | Pentex(TM) Subversion |       | False  |
    When Player 1 burns card 1 in Play region
    Then Player 1 will have 1 card in Ash heap
    And Player 4 will have 1 card in Ash heap
    And these cards will have the following state:
      | Name     | Region   | Card Position | Card Name             | Blood | Locked | Face Down |
      | Player 1 | Ash heap | 1             | Anson                 | 0     | False  | False     |
      | Player 4 | Ash heap | 1             | Pentex(TM) Subversion |       | False  | False     |

  Scenario: A card is burned incorrectly
    Given Player 1 has played card 1 from Uncontrolled to Play region
    And Player 2 has played card 1 from Hand to Ash heap
    When Player 2 burns card 1 in Ash heap
    Then there will be an error message in English: "Invalid card at Player 2's Ash heap -> [1]"

  Scenario: An ousted player tries to move a card
    Given the following pool adjustments are made:
      | Name     | Change |
      | Player 1 | -5     |
    When Player 1 moves card 1 from Hand to Play region
    Then there will be an error message in English: "Invalid command.  Player 1 is ousted."

  Scenario: A card is attached to an ousted player's card
    Given Player 2 moves card 1 from Uncontrolled to Play region
    And the following pool adjustments are made:
      | Name     | Change |
      | Player 2 | -5     |
    When Player 1 attaches card 1 from Hand to card 1 in Player 2's Play region
    Then there will be an error message in English: "Invalid command.  Player 2 is ousted."