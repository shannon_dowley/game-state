Feature: Adding and removing players

  Background:
    Given there is no game
    And a game called "Test game" has been created

  Scenario: A player is added
    When a player called Player 1 is added
    Then the game will have 1 invited players

  Scenario: Player already added
    Given Player 1 has joined the game
    When a player called Player 1 is added
    Then there will be an error message in English: "Player 1 is already in this game."

  Scenario: Removing a player
    Given Player 1 has joined the game
    And Player 2 has joined the game
    And Player 3 has joined the game
    And Player 1 has registered a valid deck
    And Player 2 has registered a valid deck
    And Player 3 has registered a valid deck
    When Player 1 is removed from the game
    Then the game will have 2 invited players

  Scenario: Removing a player who's not in the game
    Given Player 1 has joined the game
    And Player 2 has joined the game
    When Player 3 is removed from the game
    Then there will be an error message in English: "Player 3 has not yet been invited to game."

  Scenario: Removing a player in a started game
    Given Player 1 has joined the game
    And Player 2 has joined the game
    And Player 3 has joined the game
    And Player 4 has joined the game
    And Player 5 has joined the game
    And Player 1 has registered a valid deck
    And Player 2 has registered a valid deck
    And Player 3 has registered a valid deck
    And Player 4 has registered a valid deck
    And Player 5 has registered a valid deck
    And the game is started
    When Player 3 is removed from the game
    Then there will be an error message in English: "Can't remove Player 3 from a game in progress."

  Scenario: Too many players
    Given Player 1 has joined the game
    And Player 2 has joined the game
    And Player 3 has joined the game
    And Player 4 has joined the game
    And Player 5 has joined the game
    And Player 1 has registered a valid deck
    And Player 2 has registered a valid deck
    And Player 3 has registered a valid deck
    And Player 4 has registered a valid deck
    And Player 5 has registered a valid deck
    When a player called Player 6 is added
    Then there will be an error message in English: "Unable to invite Player 6.  Game is full."

  Scenario: Starting a game with not enough players
    When the game is started
    Then there will be an error message in English: "Not enough players to start game. At least 1 is required."

  Scenario: Starting the game with missing player order
    Given Player 1 has joined the game
    And Player 2 has joined the game
    And Player 3 has joined the game
    And Player 4 has joined the game
    And Player 5 has joined the game
    And Player 1 has registered a valid deck
    And Player 2 has registered a valid deck
    And Player 3 has registered a valid deck
    And Player 4 has registered a valid deck
    And Player 5 has registered a valid deck
    When the game is started with the player order:
      | Player 1 |
      | Player 3 |
      | Player 5 |
    Then there will be an error message in English: "Not all registered players ordered. Missing: [Player 2, Player 4]"

  Scenario: Starting the game with unregistered players
    Given Player 1 has joined the game
    And Player 2 has joined the game
    And Player 3 has joined the game
    And Player 4 has joined the game
    And Player 5 has joined the game
    And Player 1 has registered a valid deck
    And Player 2 has registered a valid deck
    And Player 3 has registered a valid deck
    When the game is started with the player order:
      | Player 1 |
      | Player 2 |
      | Player 3 |
      | Player 4 |
      | Player 5 |
    Then there will be an error message in English: "Unable to order game: The following players have not registered: [Player 4, Player 5]."

  Scenario: Replacing a player
    Given Player 1 has joined the game
    And Player 2 has joined the game
    And Player 3 has joined the game
    And Player 4 has joined the game
    And Player 5 has joined the game
    And Player 1 has registered a valid deck
    And Player 2 has registered a valid deck
    And Player 3 has registered a valid deck
    And Player 4 has registered a valid deck
    And Player 5 has registered a valid deck
    And the game is started
    When Player 3 is replaced with Player 6
    Then these players will have the following state:
      | Name     | Pool | Victory Points |
      | Player 1 | 30   | 0              |
      | Player 2 | 30   | 0              |
      | Player 6 | 30   | 0              |
      | Player 4 | 30   | 0              |
      | Player 5 | 30   | 0              |

  Scenario: Replacing a player with an existing player
    Given Player 1 has joined the game
    And Player 2 has joined the game
    And Player 3 has joined the game
    And Player 4 has joined the game
    And Player 5 has joined the game
    And Player 1 has registered a valid deck
    And Player 2 has registered a valid deck
    And Player 3 has registered a valid deck
    And Player 4 has registered a valid deck
    And Player 5 has registered a valid deck
    And the game is started
    When Player 3 is replaced with Player 1
    Then there will be an error message in English: "Player 1 is already in this game."

  Scenario: Replacing a non-existing player
    Given Player 1 has joined the game
    And Player 2 has joined the game
    And Player 3 has joined the game
    And Player 4 has joined the game
    And Player 5 has joined the game
    And Player 1 has registered a valid deck
    And Player 2 has registered a valid deck
    And Player 3 has registered a valid deck
    And Player 4 has registered a valid deck
    And Player 5 has registered a valid deck
    And the game is started
    When Player 6 is replaced with Player 7
    Then there will be an error message in English: "Player 6 not in game or not registered."