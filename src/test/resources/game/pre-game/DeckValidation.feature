Feature: Registering decks

  Background:
    Given there is no game
    And a game called "Test game" has been created
    And Player 1 has joined the game
    And Player 2 has joined the game
    And Player 3 has joined the game
    And Player 4 has joined the game
    And Player 5 has joined the game

  Scenario: Register a valid deck
    When Player 1 registers a library that is valid and a crypt that is valid
    Then the game will have 1 registered players

  Scenario: Registers a crypt that's too small
    When Player 1 registers a library that is valid and a crypt that is too small
    Then there will be a deck registration error in English: "Crypt too small: 4 of 12 cards required."

  Scenario: Registers a library that's too small
    When Player 1 registers a library that is too small and a crypt that is valid
    Then there will be a deck registration error in English: "Library too small: 40 of 60 cards required."

  Scenario: Registers a library that's too big
    When Player 1 registers a library that is too big and a crypt that is valid
    Then there will be a deck registration error in English: "Library too large: 91 of 90 card limit."

  Scenario: Registers a library with a banned card
    When Player 1 registers a library that is containing banned cards and a crypt that is valid
    Then there will be a deck registration error in English: "Card 'Anthelios, The Red Star' is banned."

  Scenario: Registers a invalid deck and invalid library
    When Player 1 registers a library that is too big and a crypt that is too small
    Then there will be a deck registration error in English: "Library too large: 91 of 90 card limit."
    And there will be a deck registration error in English: "Crypt too small: 4 of 12 cards required."

  Scenario: Registers a invalid crypt, not adjacent groups
    When Player 1 registers a library that is valid and a crypt that has an invalid grouping
    Then there will be a deck registration error in English: "Crypt grouping invalid: [1, 3]."

  Scenario: Registers a invalid crypt, too many groups
    When Player 1 registers a library that is valid and a crypt that has too many groups
    Then there will be a deck registration error in English: "Crypt grouping invalid: [1, 2, 3]."

  Scenario: Registers a crypt with a card id that's not found
    When Player 1 registers a library that is valid and a crypt that has an error card
    Then there will be a deck registration error in English: "Card with ID '0' not found."
    And there will be a deck registration error in English: "Crypt too small: 8 of 12 cards required."

  Scenario: Registers a crypt with a banned card
    When Player 1 registers a library that is valid and a crypt that is containing banned cards
    Then there will be a deck registration error in English: "Card 'Tsigane' is banned."

