package net.deckserver.api.game;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.deckserver.api.card.CardCount;
import net.deckserver.api.card.KrcgDeck;
import net.deckserver.api.game.command.CardEntry;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class TestConstants {

    public static String PLAYER_1_DECK = "src/test/resources/decks/player1_deck.json";
    public static String PLAYER_2_DECK = "src/test/resources/decks/player2_deck.json";
    public static String PLAYER_3_DECK = "src/test/resources/decks/player3_deck.json";
    public static String PLAYER_4_DECK = "src/test/resources/decks/player4_deck.json";
    public static String PLAYER_5_DECK = "src/test/resources/decks/player5_deck.json";
    public static String INVALID_CRYPT_TOO_SMALL = "src/test/resources/decks/invalid_crypt_too_small.json";
    public static String INVALID_CRYPT_CARD_ERROR = "src/test/resources/decks/invalid_crypt_card_error.json";
    public static String INVALID_CRYPT_BANNED = "src/test/resources/decks/invalid_crypt_banned_card.json";
    public static String INVALID_CRYPT_GROUPING = "src/test/resources/decks/invalid_crypt_grouping.json";
    public static String INVALID_CRYPT_TOO_MANY_GROUPS = "src/test/resources/decks/invalid_crypt_too_many_groups.json";
    public static String VALID_CRYPT = "src/test/resources/decks/valid_crypt.json";
    public static String INVALID_LIBRARY_TOO_SMALL = "src/test/resources/decks/invalid_library_too_small.json";
    public static String INVALID_LIBRARY_TOO_BIG = "src/test/resources/decks/invalid_library_too_big.json";
    public static String INVALID_LIBRARY_BANNED = "src/test/resources/decks/invalid_library_banned_card.json";
    public static String VALID_LIBRARY = "src/test/resources/decks/valid_library.json";

    public static List<CardEntry> loadDeckList(ObjectMapper mapper, String filePath) {
        try {
            CardEntry[] deckList = mapper.readValue(Paths.get(filePath).toFile(), CardEntry[].class);
            Arrays.sort(deckList, Comparator.comparing(CardEntry::getId));
            return Arrays.asList(deckList);
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public static KrcgDeck loadPlayerDeck(ObjectMapper mapper, String filePath) {
        try {
            return mapper.readValue(Paths.get(filePath).toFile(), KrcgDeck.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private static CardEntry map(CardCount cardCount) {
        return new CardEntry(cardCount.getId(), cardCount.getCount());
    }

    public static List<CardEntry> mapCrypt(KrcgDeck deck) {
        return deck.getCrypt().getCards().stream().map(TestConstants::map).toList();
    }

    public static List<CardEntry> mapLibrary(KrcgDeck deck) {
        return deck.getLibrary().getCards()
                .stream()
                .flatMap(typeCount -> typeCount.getCards().stream())
                .map(TestConstants::map).toList();
    }

    @Test
    public void loadDeck() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.findAndRegisterModules();
        KrcgDeck deck = loadPlayerDeck(objectMapper, PLAYER_1_DECK);
        List<CardEntry> crypt = mapCrypt(deck);
        List<CardEntry> library = mapLibrary(deck);
    }
}