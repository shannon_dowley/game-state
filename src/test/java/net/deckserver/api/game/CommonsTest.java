package net.deckserver.api.game;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CommonsTest {

    @Test
    public void testDisciplineType() {
        assertThrows(IllegalArgumentException.class, () -> DisciplineType.byCode("invalid"));
    }

    @Test
    public void testPhaseChange() {
        assertEquals(PhaseType.UNLOCK, PhaseType.DISCARD.next());
    }
}
