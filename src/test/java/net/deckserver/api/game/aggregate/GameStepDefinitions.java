package net.deckserver.api.game.aggregate;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.azam.ulidj.ULID;
import io.cucumber.java.Before;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import net.deckserver.api.card.CardService;
import net.deckserver.api.card.DummyCardService;
import net.deckserver.api.card.KrcgDeck;
import net.deckserver.api.game.PhaseType;
import net.deckserver.api.game.RegionType;
import net.deckserver.api.game.command.*;
import net.deckserver.api.game.event.GameCreated;
import net.deckserver.api.game.event.PlayerAdded;
import net.deckserver.api.game.event.RegionShuffled;
import org.axonframework.springboot.autoconfig.AxonServerAutoConfiguration;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.axonframework.test.aggregate.ResultValidator;
import org.axonframework.test.aggregate.TestExecutor;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

import static net.deckserver.api.game.TestConstants.*;
import static org.axonframework.test.matchers.Matchers.listWithAnyOf;
import static org.axonframework.test.matchers.Matchers.messageWithPayload;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@CucumberContextConfiguration
@SpringBootTest
@EnableAutoConfiguration(exclude = AxonServerAutoConfiguration.class)
public class GameStepDefinitions {

    private final UUID gameId = UUID.fromString("b25ca242-2a39-4f1b-8973-e050db611394");

    private TestExecutor<Game> test;
    private ResultValidator<Game> result;
    private String gameName;
    private final Set<String> playerMap = new HashSet<>();

    private final CardService cardService = new DummyCardService();

    @Autowired
    private ObjectMapper objectMapper;

    private final ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    FixtureConfiguration<Game> fixture = new AggregateTestFixture<>(Game.class);
    private long seed = 1L;

    @Before
    public void setup() {
        fixture.registerInjectableResource(cardService);
        messageSource.setBasenames("common", "events", "commands");
    }

    @ParameterType("(Player \\d)")
    public String player(String name) {
        return name;
    }

    @ParameterType("(\\d(?:\\.\\d)?)")
    public List<Integer> cardPosition(String position) {
        return Arrays.stream(position.split("\\.")).map(Integer::parseInt).collect(Collectors.toList());
    }

    @ParameterType("Play region|Out of play|Uncontrolled|Ash heap|Removed from Game|Crypt|Library|Hand|Research area")
    public RegionType region(String region) {
        return switch (region) {
            case "Play region" -> RegionType.IN_PLAY;
            case "Out of play" -> RegionType.OUT_OF_PLAY;
            case "Uncontrolled" -> RegionType.UNCONTROLLED;
            case "Ash heap" -> RegionType.ASH_HEAP;
            case "Removed from Game" -> RegionType.REMOVED_FROM_GAME;
            case "Crypt" -> RegionType.CRYPT;
            case "Library" -> RegionType.LIBRARY;
            case "Hand" -> RegionType.HAND;
            case "Research area" -> RegionType.RESEARCH;
            default -> throw new IllegalArgumentException("Unsupported region");
        };
    }

    @ParameterType("Unlock|Master|Minion|Influence|Discard")
    public PhaseType phase(String phase) {
        return switch (phase) {
            case "Unlock" -> PhaseType.UNLOCK;
            case "Master" -> PhaseType.MASTER;
            case "Minion" -> PhaseType.MINION;
            case "Influence" -> PhaseType.INFLUENCE;
            case "Discard" -> PhaseType.DISCARD;
            default -> throw new IllegalArgumentException("Unsupported phase");
        };
    }

    @ParameterType("English|Spanish")
    public Locale locale(String locale) {
        return switch (locale) {
            case "English" -> Locale.ENGLISH;
            case "Spanish" -> new Locale.Builder().setLanguage("ES").build();
            default -> throw new IllegalArgumentException("Unsupported language");
        };
    }

    @ParameterType("valid|too small|too many groups|an invalid grouping|an error card|containing banned cards")
    public List<CardEntry> cryptType(String type) {
        return switch (type) {
            case "valid" -> loadDeckList(objectMapper, VALID_CRYPT);
            case "too small" -> loadDeckList(objectMapper, INVALID_CRYPT_TOO_SMALL);
            case "too many groups" -> loadDeckList(objectMapper, INVALID_CRYPT_TOO_MANY_GROUPS);
            case "an invalid grouping" -> loadDeckList(objectMapper, INVALID_CRYPT_GROUPING);
            case "an error card" -> loadDeckList(objectMapper, INVALID_CRYPT_CARD_ERROR);
            case "containing banned cards" -> loadDeckList(objectMapper, INVALID_CRYPT_BANNED);
            default -> throw new IllegalArgumentException("Unsupported crypt");
        };
    }

    @ParameterType("valid|too small|too big|containing banned cards")
    public List<CardEntry> libraryType(String type) {
        return switch (type) {
            case "valid" -> loadDeckList(objectMapper, VALID_LIBRARY);
            case "too small" -> loadDeckList(objectMapper, INVALID_LIBRARY_TOO_SMALL);
            case "too big" -> loadDeckList(objectMapper, INVALID_LIBRARY_TOO_BIG);
            case "containing banned cards" -> loadDeckList(objectMapper, INVALID_LIBRARY_BANNED);
            default -> throw new IllegalArgumentException("Unsupported library");
        };
    }

    @ParameterType("gains|loses")
    public Boolean gainType(String type) {
        return switch (type) {
            case "gains" -> true;
            case "loses" -> false;
            default -> throw new IllegalArgumentException("Unsupported gain type");
        };
    }

    @ParameterType("locks|unlocks")
    public Boolean lockType(String type) {
        return switch (type) {
            case "locks" -> true;
            case "unlocks" -> false;
            default -> throw new IllegalArgumentException("Unsupported lock type");
        };
    }

    @ParameterType(".*")
    public boolean facing(String type) {
        return type.equals("face down");
    }

    @ParameterType(".*")
    public boolean bottom(String type) {
        return type.equals("the bottom of ");
    }

    @ParameterType("crypt|library")
    public boolean location(String type) {
        return switch (type) {
            case "crypt" -> true;
            case "library" -> false;
            default -> throw new IllegalArgumentException("Unsupported location type");
        };
    }

    @ParameterType("onto|from")
    public String transferDirection(String type) {
        return type;
    }

    @Given("there is no game")
    public void no_game() {
        test = fixture.givenNoPriorActivity();
    }

    @Given("this is a new game")
    public void new_game() {
        test = fixture.givenState(() -> {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.findAndRegisterModules();
                objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                return objectMapper.readValue(new File("src/test/resources/new_game.json"), Game.class);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Given("this is an existing game")
    public void existing_game() {
        test = fixture.givenState(() -> {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.findAndRegisterModules();
                objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                return objectMapper.readValue(new File("src/test/resources/existing_game.json"), Game.class);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Given("a game called {string} has been created")
    public void aGameCalledExists(String gameName) {
        this.gameName = gameName;
        test.andGiven(new GameCreated(gameId, gameName, this.seed));
    }

    @Given("{player} has joined the game")
    public void has_joined_the_game(String playerName) {
        this.playerMap.add(playerName);
        test.andGiven(new PlayerAdded(gameId, gameName, ULID.random(), playerName));
    }

    @Given("{player} has registered a valid deck")
    public void has_registered_a_valid_deck(String playerName) {
        String fileName = switch (playerName) {
            case "Player 1" -> PLAYER_1_DECK;
            case "Player 2" -> PLAYER_2_DECK;
            case "Player 3" -> PLAYER_3_DECK;
            case "Player 4" -> PLAYER_4_DECK;
            case "Player 5" -> PLAYER_5_DECK;
            default -> throw new IllegalArgumentException("Invalid player name");
        };
        KrcgDeck deck = loadPlayerDeck(objectMapper, fileName);
        List<CardEntry> crypt = mapCrypt(deck);
        List<CardEntry> library = mapLibrary(deck);
        test.andGivenCommands(new RegisterDeck(gameId, playerName, new DeckList(crypt, library)));
    }

    @Given("the game has already been started")
    public void the_game_has_been_started() {
        List<String> playerOrder = new ArrayList<>(this.playerMap);
        test.andGivenCommands(new StartGame(gameId, playerOrder));
    }

    @Given("{player} has played card {cardPosition} from {region} to {bottom}{region}{facing}")
    public void a_card_has_been_played(String playerName, List<Integer> positions, RegionType source, boolean bottom, RegionType target, boolean faceDown) {
        TargetCard sourceCard = new TargetCard(playerName, source, positions);
        TargetRegion targetRegion = new TargetRegion(playerName, target);
        test.andGivenCommands(new MoveCard(gameId, sourceCard, targetRegion, !bottom, faceDown));
    }

    @Given("{player} has attached card {cardPosition} from {region} to card {cardPosition} in {player}'s {region}{facing}")
    public void a_card_has_been_attached(String playerName, List<Integer> sourcePositions, RegionType sourceRegion, List<Integer> targetPositions, String targetPlayer, RegionType targetRegion, boolean faceDown) {
        TargetCard sourceCard = new TargetCard(playerName, sourceRegion, sourcePositions);
        TargetCard targetCard = new TargetCard(targetPlayer, targetRegion, targetPositions);
        test.andGivenCommands(new AttachCard(gameId, sourceCard, targetCard, false, faceDown));
    }

    @Given("the turn has progressed to the {phase} phase")
    public void a_game_is_in_phase(PhaseType phase) {
        List<EndPhase> phaseCommands = Collections.nCopies(phase.ordinal(), new EndPhase(gameId));
        test.andGivenCommands(phaseCommands);
    }

    @When("a game called {string} is created")
    public void game_is_created(String gameName) {
        result = test.when(new CreateGame(gameId, gameName));
    }

    @When("a player called {player} is added")
    public void player_is_added(String playerName) {
        this.playerMap.add(playerName);
        result = test.when(new AddPlayer(gameId, playerName));
    }

    @When("{player} registers a library that is/has {libraryType} and a crypt that is/has {cryptType}")
    public void register_a_deck(String playerName, List<CardEntry> libraryType, List<CardEntry> cryptType) {
        result = test.when(new RegisterDeck(gameId, playerName, new DeckList(cryptType, libraryType)));
    }

    @When("{player} is removed from the game")
    public void is_removed_from_the_game(String playerName) {
        result = test.when(new RemovePlayer(gameId, playerName));
    }

    @When("the game is started")
    public void the_game_is_started() {
        List<String> playerOrder = this.playerMap.stream().sorted().collect(Collectors.toList());
        result = test.when(new StartGame(gameId, playerOrder));
    }

    @When("the game is started with the player order:")
    public void the_game_is_started_with_the_player_order(List<String> playerNames) {
        result = test.when(new StartGame(this.gameId, playerNames));
    }

    @When("{player} shuffles {region}")
    public void shuffles_their_region(String playerName, RegionType region) {
        if (region == RegionType.LIBRARY) {
            result = test.when(new ShuffleLibrary(this.gameId, playerName));
        } else {
            result = test.when(new ShuffleCrypt(this.gameId, playerName));
        }
    }

    @When("{player} draws {int} library card(s)")
    public void draws_a_library_card(String playerName, int amount) {
        result = test.when(new DrawLibraryCard(this.gameId, playerName, amount));
    }

    @When("{player} draws {int} crypt card(s)")
    public void draws_a_crypt_card(String playerName, Integer amount) {
        result = test.when(new DrawCryptCard(this.gameId, playerName, amount));
    }

    @When("the following pool adjustments are made:")
    public void the_following_pool_adjustments_are_made(List<Map<String, String>> playerAdjustments) {
        List<TargetPlayerAmount> adjustments = new ArrayList<>();
        for (var adjustment : playerAdjustments) {
            String playerName = adjustment.get("Name");
            int amount = Integer.parseInt(adjustment.get("Change"));
            adjustments.add(new TargetPlayerAmount(playerName, amount));
        }
        result = test.when(new AdjustPool(this.gameId, adjustments));
    }

    @When("{player} moves card {cardPosition} from {region} to {bottom}{region}{facing}")
    public void move_card_to_region(String playerName, List<Integer> positions, RegionType source, boolean bottom, RegionType target, boolean faceDown) {
        TargetCard sourceCard = new TargetCard(playerName, source, positions);
        TargetRegion targetRegion = new TargetRegion(playerName, target);
        result = test.when(new MoveCard(this.gameId, sourceCard, targetRegion, !bottom, faceDown));
    }

    @When("{player} attaches card {cardPosition} from {region} to card {cardPosition} in {player}'s {region}{facing}")
    public void attach_card_to_another_card(String playerName, List<Integer> sourcePosition, RegionType sourceRegion, List<Integer> targetPosition, String targetPlayer, RegionType targetRegion, boolean faceDown) {
        TargetCard sourceCard = new TargetCard(playerName, sourceRegion, sourcePosition);
        TargetCard targetCard = new TargetCard(targetPlayer, targetRegion, targetPosition);
        result = test.when(new AttachCard(this.gameId, sourceCard, targetCard, false, faceDown));
    }

    @When("{player} is replaced with {player}")
    public void replace_player(String playerName, String newPlayerName) {
        result = test.when(new ReplacePlayer(this.gameId, playerName, newPlayerName));
    }

    @When("{player} {lockType} card {cardPosition} in {region}")
    public void adjust_card(String playerName, Boolean lock, List<Integer> sourcePosition, RegionType region) {
        TargetCard targetCard = new TargetCard(playerName, region, sourcePosition);
        if (lock) {
            result = test.when(new LockCard(gameId, targetCard));
        } else {
            result = test.when(new UnlockCard(gameId, targetCard));
        }
    }

    @When("{player} unlocks all their cards")
    public void unlock_all(String playerName) {
        result = test.when(new UnlockAll(gameId, playerName));
    }

    @When("{player} burns card {cardPosition} in {region}")
    public void burn_card(String playerName, List<Integer> sourcePosition, RegionType region) {
        TargetCard targetCard = new TargetCard(playerName, region, sourcePosition);
        result = test.when(new BurnCard(gameId, targetCard));
    }

    @When("the current phase is ended")
    public void end_phase() {
        result = test.when(new EndPhase(gameId));
    }

    @When("{player} transfers {int} blood {transferDirection} card {cardPosition}")
    public void transfer_blood(String playerName, int amount, String direction, List<Integer> position) {
        TargetCardAmount targetCard = new TargetCardAmount(playerName, RegionType.UNCONTROLLED, position, amount);
        if (direction.equals("onto")) {
            result = test.when(new TransferOntoCrypt(this.gameId, targetCard));
        } else {
            result = test.when(new TransferOffCrypt(this.gameId, targetCard));
        }
    }

    @Then("the game name will be {string}")
    public void check_game_name(String gameName) {
        result.expectState(game -> assertThat(game.name, is(gameName)))
                .expectState(game -> assertThat(game.id, is(gameId)));
    }

    @Then("the game will have {int} invited players")
    public void check_invited_player_size(int size) {
        result.expectState(game -> assertThat(game.playerIdMap.size(), is(size)));
    }

    @Then("the game will have {int} registered players")
    public void check_registered_player_size(Integer size) {
        result.expectState(game -> assertThat(game.playerMap.size(), is(size)));
    }

    @Then("there will be an error message in {locale}: {string}")
    public void check_error_message(Locale locale, String error) {
        result.expectException(allOf(isA(GameException.class), message(is(error), locale)));
    }

    @Then("there will be a deck registration error in {locale}: {string}")
    public void check_registration_message(Locale locale, String error) {
        result.expectException(allOf(isA(IllegalDeckException.class), errors(hasItem(error), locale)));
    }

    @Then("the {region} of {player} will be shuffled")
    public void check_region_shuffle(RegionType region, String playerName) {
        result.expectEventsMatching(listWithAnyOf(messageWithPayload(equalTo(new RegionShuffled(this.gameId, playerName, region)))));
    }

    @Then("{player} will have {int} card(s) in {region}")
    public void check_region_count(String playerName, Integer amount, RegionType region) {
        result.expectState(game -> assertThat(game.findPlayer(playerName).getRegion(region).count(), is(amount)));
    }

    @Then("{player} will have {int} pool and {float} VP")
    public void check_pool_and_vp(String playerName, Integer pool, Float victoryPoints) {
        result.expectState(game -> {
            assertThat(game.findPlayer(playerName).getPool(), is(pool));
            assertThat(game.findPlayer(playerName).getVictoryPoints(), is(victoryPoints));
        });
    }

    @Then("the next player will be {player}")
    public void check_next_player(String playerName) {
        result.expectState(game -> {
            assertThat(game.nextPlayer.getName(), is(playerName));
        });
    }

    @Then("these players will have the following state:")
    public void check_game_state(List<Map<String, String>> playerStates) {
        for (Map<String, String> state : playerStates) {
            String playerName = state.get("Name");
            String pool = state.get("Pool");
            String victoryPoints = state.get("Victory Points");
            String ousted = state.get("Ousted");
            String transfers = state.get("Transfers");
            result.expectState(game -> {
                Player player = game.findPlayer(playerName);
                testState(pool, player.getPool());
                testState(victoryPoints, player.getVictoryPoints());
                testState(ousted, player.isOusted());
                testState(transfers, game.transfers);
            });
        }
        result.expectState(game -> {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            try {
                objectMapper.writeValue(new File("target/output.json"), game);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Then("{player} will have {int} pool")
    public void check_pool_amount(String playerName, Integer amount) {
        // Write code here that turns the phrase above into concrete actions
        result.expectState(game -> assertThat(amount, is(game.findPlayer(playerName).getPool())));
    }

    @Then("{player} will have card in {region} position {int} with {int} card/s attached")
    public void check_card_attached_size(String playerName, RegionType targetRegion, int target, int count) {
        // Write code here that turns the phrase above into concrete actions
        result.expectState(game -> assertThat(count, is(game.findPlayer(playerName).getRegion(targetRegion).getCard(target).count())));
    }

    @Then("the current player will be {player}")
    public void check_current_player(String playerName) {
        result.expectState(game -> assertThat(playerName, is(game.currentPlayer.getName())));
    }

    @Then("these cards (will have)(are in) the following state:")
    public void these_cards_will_have_the_following_state(List<Map<String, String>> cardStates) {
        if (result == null) {
            result = test.whenTimeElapses(Duration.ZERO);
        }
        for (Map<String, String> state : cardStates) {
            String playerName = state.get("Name");
            String cardName = state.get("Card Name");
            RegionType regionType = region(state.get("Region"));
            String capacity = state.get("Capacity");
            String blood = state.get("Blood");
            List<Integer> position = parsePosition(state.get("Card Position"));
            Boolean locked = Boolean.parseBoolean(state.get("Locked"));
            String faceDown = state.get("Face Down");
            result.expectState(game -> {
                Region region = game.findPlayer(playerName).getRegion(regionType);
                GameCard card = game.find(playerName, region, position);
                assertEquals(locked, card.isLocked());
                testState(cardName, card.getName());
                testState(faceDown, card.isFaceDown());
                testState(capacity, card.getCapacity());
                testState(blood, card.getCount(CounterType.BLOOD));
            });
        }
    }

    @Then("the current phase will be the {phase} phase")
    public void the_game_will_have_the_following_phase(PhaseType phase) {
        result.expectState(state -> assertThat(state.phase, is(phase)));
    }

    private List<Integer> parsePosition(String position) {
        return Arrays.stream(position.split("\\.")).map(Integer::parseInt).collect(Collectors.toList());
    }

    private void testState(String field, String expected) {
        if (field != null && !field.isEmpty()) {
            assertEquals(field, expected);
        }
    }

    private void testState(String field, Float expected) {
        if (field != null && !field.isEmpty()) {
            assertEquals(Float.parseFloat(field), expected);
        }
    }

    private void testState(String field, Integer expected) {
        if (field != null && !field.isEmpty()) {
            assertEquals(Integer.parseInt(field), expected);
        }
    }

    private void testState(String field, Boolean expected) {
        if (field != null && !field.isEmpty()) {
            assertEquals(Boolean.parseBoolean(field), expected);
        }
    }

    private FeatureMatcher<GameException, String> message(Matcher<String> matcher, Locale locale) {
        return new FeatureMatcher<>(matcher, "localizedMessage", "localizedMessage") {
            @Override
            protected String featureValueOf(GameException actual) {
                Object[] resolvedArgs = Arrays.stream(actual.getArgs()).map(this::resolveArgument).toArray();
                return messageSource.getMessage(actual.getClass().getSimpleName(), resolvedArgs, locale);
            }

            private Object resolveArgument(Object arg) {
                try {
                    return messageSource.getMessage(new DefaultMessageSourceResolvable(new String[]{arg.getClass().getSimpleName(), arg.toString()}), locale);
                } catch (NoSuchMessageException e) {
                    return arg;
                }
            }
        };
    }

    private FeatureMatcher<IllegalDeckException, List<String>> errors(Matcher<Iterable<? super String>> matcher, Locale locale) {
        return new FeatureMatcher<>(matcher, "hasErrors", "hasErrors") {
            @Override
            protected List<String> featureValueOf(IllegalDeckException actual) {
                return actual.getErrors().stream().map(this::generateError).collect(Collectors.toList());
            }

            private String generateError(DeckError deckError) {
                return messageSource.getMessage(deckError.getClass().getSimpleName(), deckError.getArgs(), locale);
            }
        };
    }

}
