package net.deckserver.api.game.aggregate;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.axonframework.springboot.autoconfig.AxonServerAutoConfiguration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@EnableAutoConfiguration(exclude = AxonServerAutoConfiguration.class)
public class GameStorageTest {

    @Test
    public void testLoad() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.findAndRegisterModules();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Game game = objectMapper.readValue(new File("src/test/resources/existing_game.json"), Game.class);
        assertNotNull(game);
        assertEquals(5, game.playerMap.size());
        Set<Integer> cardHashes = new HashSet<>();
        for (GameCard card : game.cardMap.values()) {
            int hasCode = card.hashCode();
            assertFalse(cardHashes.contains(hasCode));
            cardHashes.add(hasCode);
            assertEquals("GameCard{name='" + card.getName() + "', id='" + card.getId() + "'}", card.toString());
        }
        Set<Integer> playerHashes = new HashSet<>();
        for (Player player : game.playerMap.values()) {
            int hashCode = player.hashCode();
            assertFalse(playerHashes.contains(hashCode));
            playerHashes.add(hashCode);
            assertEquals("Player{id='" + player.getId() + "', name='" + player.getName() + "'}", player.toString());
        }
        for (Region region : game.currentPlayer.getRegions().values()) {
            assertEquals("Region{type='" + region.getDefinition().getType() + "'}", region.toString());
        }
    }
}
