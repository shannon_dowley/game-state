package net.deckserver.api.game.aggregate;

import net.deckserver.api.game.DisciplineType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GameInternalsTest {

    @Test
    public void testRegionDefinition() {
        assertThrows(IllegalArgumentException.class, () -> RegionDefinition.byType(null));
    }

    @Test
    public void testDisciplineToSTring() {
        Discipline discipline = new Discipline(DisciplineType.BLOOD_SORCERY, DisciplineLevel.SUPERIOR);
        assertEquals("THA", discipline.toString());
    }
}
