package net.deckserver.api.card;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class DummyCardService implements CardService {

    Map<String, CardDetail> cards = new HashMap<>();

    public DummyCardService() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String filePath = "src/test/resources/decks/cards.json";
            CardEntry[] entries = objectMapper.readValue(Paths.get(filePath).toFile(), CardEntry[].class);
            for (CardEntry entry : entries) {
                Integer group = entry.getGroup() == null || entry.getGroup().equals("ANY") ? null : Integer.parseInt(entry.getGroup());
                CardDetail detail = new CardDetail(entry.getId(), entry.getDisplayName(), group, entry.getDisciplines(), entry.getCapacity(), entry.isBanned());
                cards.put(entry.getId(), detail);
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to parse local cards file", e);
        }
    }

    @NotNull
    @Override
    public CardDetail getCard(String id) throws CardNotFoundException {
        return Optional.ofNullable(cards.get(id))
                .orElseThrow(() -> new CardNotFoundException("Card '\" + id + \"' not found"));
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class CardEntry {

        private String id;
        private String displayName;
        private String group;
        private List<String> disciplines;

        private boolean banned = false;

        private Integer capacity;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public List<String> getDisciplines() {
            return disciplines;
        }

        public void setDisciplines(List<String> disciplines) {
            this.disciplines = disciplines;
        }

        public Integer getCapacity() {
            return capacity;
        }

        public void setCapacity(Integer capacity) {
            this.capacity = capacity;
        }

        public boolean isBanned() {
            return banned;
        }

        public void setBanned(boolean banned) {
            this.banned = banned;
        }
    }
}