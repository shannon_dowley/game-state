package net.deckserver.api.card

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

data class CardCount(val id: String, val name: String, val count: Int, val comments: String?)
data class CardTypeCount(val count: Int, val type: String, val cards: List<CardCount>)
data class Library(val count: Int, val cards: List<CardTypeCount>)
data class Crypt(val count: Int, val cards: List<CardCount>)
@JsonIgnoreProperties(ignoreUnknown = true)
data class KrcgDeck(val crypt: Crypt, val library: Library)