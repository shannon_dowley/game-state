package net.deckserver.api.card;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.deckserver.api.game.TestConstants;
import net.deckserver.api.game.command.CardEntry;
import org.axonframework.springboot.autoconfig.AxonServerAutoConfiguration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@EnableAutoConfiguration(exclude = AxonServerAutoConfiguration.class)
public class ApiCardServiceTest {

    private final CardService cardService = new ApiCardService();

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void testKrcg() {
        assertThrows(CardNotFoundException.class, () -> cardService.getCard("0"));
    }

    @Test
    public void loadCard() throws CardNotFoundException {
        CardDetail card = cardService.getCard("200049 ");
        assertNotNull(card);
        assertEquals(11, card.getCapacity());
    }

    @Test
    public void loadDeck() {
        List<CardEntry> entries = TestConstants.loadDeckList(mapper, "src/test/resources/decks/invalid_crypt_too_small.json");
        assertEquals(1, entries.size());
    }
}